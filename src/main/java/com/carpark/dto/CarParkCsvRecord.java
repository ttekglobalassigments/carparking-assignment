package com.carpark.dto;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;

@Data
public class CarParkCsvRecord {

  @CsvBindByName(column = "car_park_no")
  private String carParkNumber;

  @CsvBindByName(column = "address")
  private String address;

  @CsvBindByName(column = "x_coord")
  private Double latitude;

  @CsvBindByName(column = "y_coord")
  private Double longitude;
}

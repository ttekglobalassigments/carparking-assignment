package com.carpark.dto;

import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CarParkDTO {

  private String carParkNumber;

  private String address;

  private LocalDateTime updatedDate;

  @DecimalMin(value = "-90", message = "Latitude must be between -90 and 90")
  @DecimalMax(value = "90", message = "Latitude must be between -90 and 90")
  @NotNull(message = "Latitude is required and can't be null")
  private Double latitude;

  @DecimalMin(value = "-180", message = "Longitude must be between -180 and 180")
  @DecimalMax(value = "180", message = "Longitude must be between -180 and 180")
  @NotNull(message = "Longitude is required and can't be null")
  private Double longitude;

  private int totalLots;

  private int availableLots;

  private double distance;
}

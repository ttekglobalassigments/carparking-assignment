package com.carpark.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarParkLotDTO {

  @JsonProperty("items")
  private List<Item> items;

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class Item {

    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("carpark_data")
    private List<CarParkData> carParkData;
  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class CarParkData {

    @JsonProperty("carpark_number")
    private String carParkNumber;
    @JsonProperty("update_datetime")
    private String updateDatetime;
    @JsonProperty("carpark_info")
    private List<CarParkInfo> carParkInfo;
  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class CarParkInfo {

    @JsonProperty("total_lots")
    private int totalLots;
    @JsonProperty("lot_type")
    private String lotType;
    @JsonProperty("lots_available")
    private int lotsAvailable;
  }
}

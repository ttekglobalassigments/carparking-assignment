package com.carpark.model;

import com.carpark.dto.CarParkDTO;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class CarPark {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String carParkNumber;

  private String address;

  private LocalDateTime updatedDate;

  private double latitude;

  private double longitude;

  private int totalLots;

  private int availableLots;

  @Transient
  private double distance;

  public CarParkDTO convertToDTO() {
    CarParkDTO carParkDTO = new CarParkDTO();
    carParkDTO.setCarParkNumber(this.getCarParkNumber());
    carParkDTO.setUpdatedDate(this.getUpdatedDate());
    carParkDTO.setAddress(this.getAddress());
    carParkDTO.setLatitude(this.getLatitude());
    carParkDTO.setLongitude(this.getLongitude());
    carParkDTO.setTotalLots(this.getTotalLots());
    carParkDTO.setAvailableLots(this.getAvailableLots());
    carParkDTO.setDistance(this.getDistance());
    return carParkDTO;
  }

}
package com.carpark.service;

import com.carpark.dto.CarParkLotDTO;
import com.carpark.repository.CarParkRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaConsumerServiceImpl implements KafkaConsumerService {

  private CarParkRepository carParkRepository;
  private CarParkService carParkService;

  private ObjectMapper objectMapper;
  public KafkaConsumerServiceImpl(CarParkRepository carParkRepository,
      CarParkService carParkService) {
    this.carParkRepository = carParkRepository;
    this.carParkService = carParkService;
    this.objectMapper = new ObjectMapper();
  }

  @Override
  @KafkaListener(topics = {"${spring.kafka.consumer.kafka.topic}"},
      containerFactory = "kafkaListenerContainerFactory", groupId = "car-parking")
  public void consumeCarParkLotUpdatedEvent(ConsumerRecord<String, String> consumerRecord,
      Acknowledgment acknowledgment) {
    try {
      String kafkaEventDTOString = consumerRecord.value();
      CarParkLotDTO carParkLotDTO = objectMapper.readValue(kafkaEventDTOString,
          CarParkLotDTO.class);
       carParkService.manipulateCarParkDTOAndUpdateCarParkLots(carParkLotDTO);
    } catch (Exception ex) {
      log.error("Something went wrong during Kafka Consumer Car Park Lot update event", ex);
    }
  }
}

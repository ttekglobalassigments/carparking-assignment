package com.carpark.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.support.Acknowledgment;

public interface KafkaConsumerService {

  void consumeCarParkLotUpdatedEvent(ConsumerRecord<String, String> consumerRecord,
      Acknowledgment acknowledgment);
}

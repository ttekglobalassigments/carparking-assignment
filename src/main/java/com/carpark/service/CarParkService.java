package com.carpark.service;

import com.carpark.dto.CarParkDTO;
import com.carpark.dto.CarParkLotDTO;

import java.util.List;

public interface CarParkService {

  List<CarParkDTO> findNearestCarParks(double latitude, double longitude, int page, int perPage);

  void fetchCarParkLotDataAndUpdateToDatabase();

  public void manipulateCarParkDTOAndUpdateCarParkLots(CarParkLotDTO carParkLotDTO);
}

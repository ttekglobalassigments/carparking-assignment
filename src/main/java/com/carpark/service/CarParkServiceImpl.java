package com.carpark.service;

import com.carpark.dto.CarParkDTO;
import com.carpark.dto.CarParkLotDTO;
import com.carpark.model.CarPark;
import com.carpark.repository.CarParkRepository;
import com.carpark.util.CSVManipulatorUtil;
import com.carpark.util.CoordinatesDifferenceCalculatorUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CarParkServiceImpl implements CarParkService {

  private final CarParkRepository carParkRepository;
  private RestTemplate restTemplate;
  private final ObjectMapper objectMapper;
  private final CSVManipulatorUtil csvManipulator;

  private final CoordinatesDifferenceCalculatorUtil coordinatesDifferenceCalculatorUtil;

  @Value("${carpark.api.url}")
  private String carParkAPIURL;

  @Autowired
  public CarParkServiceImpl(CarParkRepository carParkRepository, RestTemplate restTemplate,
      ObjectMapper objectMapper, CSVManipulatorUtil csvManipulator,
      CoordinatesDifferenceCalculatorUtil coordinatesDifferenceCalculatorUtil) {
    this.carParkRepository = carParkRepository;
    this.restTemplate = restTemplate;
    this.objectMapper = objectMapper;
    this.csvManipulator = csvManipulator;
    this.coordinatesDifferenceCalculatorUtil = coordinatesDifferenceCalculatorUtil;
  }

  @Override
  public List<CarParkDTO> findNearestCarParks(double latitude, double longitude, int page,
      int perPage) {
    return carParkRepository.fetchAvailableParkingLots().stream()
        .peek(carPark -> {
          carPark.setDistance(
              coordinatesDifferenceCalculatorUtil.calculateDistance(latitude, longitude,
                  carPark.getLatitude(), carPark.getLongitude()));
        })
        .sorted(Comparator.comparingDouble(CarPark::getDistance)).skip((long) (page - 1) * perPage)
        .limit(perPage)
        .map(carPark -> carPark.convertToDTO()).collect(Collectors.toList());
  }

  public void fetchCarParkLotDataAndUpdateToDatabase() {
    ResponseEntity<String> responseEntity = restTemplate.getForEntity(carParkAPIURL, String.class);
    log.info("API response received");
    if (responseEntity.getStatusCode() == HttpStatus.OK) {
      try {
        CarParkLotDTO carParkLotDTO = new ObjectMapper().readValue(responseEntity.getBody(),
            CarParkLotDTO.class);
        manipulateCarParkDTOAndUpdateCarParkLots(carParkLotDTO);
      } catch (JsonProcessingException e) {
        log.error("Something went wrong while manipulating Json response of API", e);
      }
    } else {
      log.error("API call failed with status code: " + responseEntity.getStatusCodeValue());
    }
  }

  public void manipulateCarParkDTOAndUpdateCarParkLots(CarParkLotDTO carParkLotDTO) {
    carParkLotDTO.getItems().forEach(item -> item.getCarParkData().forEach(carParkData -> {
      AtomicInteger availableLots = new AtomicInteger();
      AtomicInteger totalLots = new AtomicInteger();
      carParkData.getCarParkInfo().forEach(carParkInfo -> {
        availableLots.addAndGet(carParkInfo.getLotsAvailable());
        totalLots.addAndGet(carParkInfo.getTotalLots());
      });
      updateCarParkLots(carParkData.getCarParkNumber(), totalLots.get(),
          availableLots.get(), carParkData.getUpdateDatetime());
    }));
  }

  @PostConstruct
  @Transactional
  public void loadDataFromCSVToDB() {
    if (carParkRepository.count() > 0) {
      return;
    }

    List<CarPark> carParkList = csvManipulator.extractCarParkDataFromCSV();
    if (!carParkList.isEmpty()) {
      carParkRepository.saveAll(carParkList);
    }
  }

  private void updateCarParkLots(String carParkNumber, int totalLots, int availableLots,
      String updateDatetime) {
    // Retrieve the corresponding CarPark entity from the database
    CarPark carPark = carParkRepository.findByCarParkNumber(carParkNumber);
    if (carPark != null) {
      carPark.setTotalLots(totalLots);
      carPark.setAvailableLots(availableLots);
      carPark.setUpdatedDate(LocalDateTime.parse(updateDatetime));
      carParkRepository.save(carPark);
    }
  }
}

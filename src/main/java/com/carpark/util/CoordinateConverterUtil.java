package com.carpark.util;

import java.util.HashMap;
import java.util.Map;
import org.osgeo.proj4j.CRSFactory;
import org.osgeo.proj4j.CoordinateReferenceSystem;
import org.osgeo.proj4j.ProjCoordinate;

public class CoordinateConverterUtil {

  public static Map<String, Double> convert(double svy21X, double svy21Y) {
    CRSFactory factory = new CRSFactory();
    CoordinateReferenceSystem svy21 = factory.createFromName("EPSG:3414"); // SVY21 EPSG code

    // Perform the coordinate transformation
    ProjCoordinate svy21Coordinates = new ProjCoordinate(svy21X, svy21Y);
    ProjCoordinate wgs84Coordinates = new ProjCoordinate();
    svy21.getProjection().inverseProject(svy21Coordinates, wgs84Coordinates);

    double roundedLatitude = Math.round(wgs84Coordinates.y * 1e5) / 1e5;
    double roundedLongitude = Math.round(wgs84Coordinates.x * 1e3) / 1e3;

    Map<String, Double> coordinates = new HashMap<>();
    coordinates.put("latitude", roundedLatitude);
    coordinates.put("longitude", roundedLongitude);

    return coordinates;
  }
}

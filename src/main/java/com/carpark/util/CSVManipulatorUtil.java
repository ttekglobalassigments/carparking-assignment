package com.carpark.util;

import com.carpark.dto.CarParkCsvRecord;
import com.carpark.model.CarPark;
import com.carpark.service.CarParkServiceImpl;
import com.opencsv.bean.CsvToBeanBuilder;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CSVManipulatorUtil {

  public List<CarPark> extractCarParkDataFromCSV() {
    ClassLoader classLoader = CarParkServiceImpl.class.getClassLoader();
    InputStream inputStream = classLoader.getResourceAsStream("HDBCarparkInformation.csv");
    List<CarPark> carParkList = new ArrayList<>();
    InputStreamReader inputStreamReader = new InputStreamReader(
        Objects.requireNonNull(inputStream));
    List<CarParkCsvRecord> records = new CsvToBeanBuilder<CarParkCsvRecord>(inputStreamReader)
        .withType(CarParkCsvRecord.class)
        .build()
        .parse();
    carParkList = records.stream().map(this::mapToCarPark).toList();
    return carParkList;
  }

  private CarPark mapToCarPark(CarParkCsvRecord carParkCsvRecord) {
    Map<String, Double> coordinates = CoordinateConverterUtil.convert(
        carParkCsvRecord.getLatitude(), carParkCsvRecord.getLongitude());

    return CarPark.builder()
        .carParkNumber(carParkCsvRecord.getCarParkNumber())
        .address(carParkCsvRecord.getAddress())
        .latitude(coordinates.get("latitude"))
        .longitude(coordinates.get("longitude"))
        .totalLots(0)
        .availableLots(0)
        .build();
  }
}

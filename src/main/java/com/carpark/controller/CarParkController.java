package com.carpark.controller;

import com.carpark.dto.CarParkDTO;
import com.carpark.service.CarParkService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/carparks")
@Validated
public class CarParkController {

  private final CarParkService carParkService;

  public CarParkController(CarParkService carParkService) {
    this.carParkService = carParkService;
  }

  @GetMapping("/nearest")
  public List<CarParkDTO> getNearestCarParks(
      @Valid CarParkDTO carParkDTO,
      @RequestParam("page") int page,
      @RequestParam("per_page") int perPage) {

    return carParkService.findNearestCarParks(carParkDTO.getLatitude(), carParkDTO.getLongitude(),
        page, perPage);
  }

  @PutMapping("/updateCarParkLots")
  public ResponseEntity<String> updateCarParkLotInfo() {
    carParkService.fetchCarParkLotDataAndUpdateToDatabase();
    return ResponseEntity.ok("Car Park Lot Data updated successfully");
  }
}

package com.carpark.repository;

import com.carpark.model.CarPark;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface CarParkRepository extends CrudRepository<CarPark, Long>,
    PagingAndSortingRepository<CarPark, Long> {

  CarPark findByCarParkNumber(String carParkNumber);
  
  @Query("SELECT cp FROM CarPark cp WHERE CAST(cp.availableLots AS integer) > 0")
  List<CarPark> fetchAvailableParkingLots();
}

package com.carpark.service;

import com.carpark.dto.CarParkDTO;
import com.carpark.model.CarPark;
import com.carpark.repository.CarParkRepository;
import com.carpark.util.CSVManipulatorUtil;
import com.carpark.util.CoordinatesDifferenceCalculatorUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
public class CarParkServiceTest {

  @InjectMocks
  CarParkServiceImpl carParkService;

  @Mock
  CarParkRepository carParkRepository;

  @Mock
  RestTemplate restTemplate;

  @Mock
  ObjectMapper objectMapper;

  @Mock
  CSVManipulatorUtil csvManipulator;

  @Mock
  CoordinatesDifferenceCalculatorUtil coordinatesDifferenceCalculatorUtil;

  List<CarPark> carParkList;

  @BeforeAll
  public void setUp() {
    carParkService = new CarParkServiceImpl(carParkRepository, restTemplate,
        objectMapper, csvManipulator, coordinatesDifferenceCalculatorUtil);

    CarPark carPark1 = CarPark.builder().carParkNumber("HG29").address("Address 1").totalLots(100)
        .availableLots(20).latitude(1.37234).longitude(103.899).build();
    CarPark carPark2 = CarPark.builder().carParkNumber("HG47").address("Address 2").totalLots(150)
        .availableLots(15).latitude(1.37118).longitude(103.894).build();
    CarPark carPark3 = CarPark.builder().carParkNumber("HG30").address("Address 3").totalLots(110)
        .availableLots(40).latitude(1.37326).longitude(103.897).build();
    CarPark carPark5 = CarPark.builder().carParkNumber("HG14").address("Address 5").totalLots(70)
        .availableLots(40).latitude(1.3692).longitude(103.896).build();
    CarPark carPark6 = CarPark.builder().carParkNumber("HG97").address("Address 6").totalLots(60)
        .availableLots(25).latitude(1.37122).longitude(103.895).build();

    carParkList = Arrays.asList(carPark1, carPark2, carPark3, carPark5, carPark6);
  }

  @Test
  public void testFindNearestCarParks_whenCarkParksAvailable_returnNearestCarParks() {

    CarParkDTO carParkDTO1 = CarParkDTO.builder().carParkNumber("HG30").address("Address 3")
        .totalLots(110).availableLots(40).latitude(1.37326).longitude(103.897).distance(0.0)
        .build();
    CarParkDTO carParkDTO2 = CarParkDTO.builder().carParkNumber("HG29").address("Address 1")
        .totalLots(100).availableLots(20).latitude(1.37234).longitude(103.899)
        .distance(0.2447325346148452).build();
    CarParkDTO carParkDTO3 = CarParkDTO.builder().carParkNumber("HG97").address("Address 6")
        .totalLots(60).availableLots(25).latitude(1.37122).longitude(103.895)
        .distance(0.31762273664157764).build();
    CarParkDTO carParkDTO4 = CarParkDTO.builder().carParkNumber("HG47").address("Address 2")
        .totalLots(150).availableLots(15).latitude(1.37118).longitude(103.894)
        .distance(0.4058422689624116).build();
    CarParkDTO carParkDTO5 = CarParkDTO.builder().carParkNumber("HG14").address("Address 5")
        .totalLots(70).availableLots(40).latitude(1.3692).longitude(103.896)
        .distance(0.46493612438615356).build();

    setCoordinateDifferenceExpectations();
    Mockito.when(carParkRepository.fetchAvailableParkingLots()).thenReturn(carParkList);

    int page = 1;
    int per_Page = 5;

    List<CarParkDTO> actualCarParkDTOList = carParkService.findNearestCarParks(1.37326, 103.897,
        page, per_Page);
    List<CarParkDTO> expectedCarParkDTOList = Arrays.asList(carParkDTO1, carParkDTO2, carParkDTO3,
        carParkDTO4, carParkDTO5);

    Assertions.assertEquals(expectedCarParkDTOList.size(), actualCarParkDTOList.size());
    Assertions.assertEquals(expectedCarParkDTOList, actualCarParkDTOList);
  }

    private void setCoordinateDifferenceExpectations() {
    Mockito.when(coordinatesDifferenceCalculatorUtil.calculateDistance(1.37326, 103.897,
            carParkList.get(0).getLatitude(), carParkList.get(0).getLongitude()))
        .thenReturn(0.2447325346148452);

    Mockito.when(coordinatesDifferenceCalculatorUtil.calculateDistance(1.37326, 103.897,
            carParkList.get(1).getLatitude(), carParkList.get(1).getLongitude()))
        .thenReturn(0.4058422689624116);

    Mockito.when(coordinatesDifferenceCalculatorUtil.calculateDistance(1.37326, 103.897,
            carParkList.get(2).getLatitude(), carParkList.get(2).getLongitude()))
        .thenReturn(0.0);

    Mockito.when(coordinatesDifferenceCalculatorUtil.calculateDistance(1.37326, 103.897,
            carParkList.get(3).getLatitude(), carParkList.get(3).getLongitude()))
        .thenReturn(0.46493612438615356);

    Mockito.when(coordinatesDifferenceCalculatorUtil.calculateDistance(1.37326, 103.897,
            carParkList.get(4).getLatitude(), carParkList.get(4).getLongitude()))
        .thenReturn(0.31762273664157764);
  }
}

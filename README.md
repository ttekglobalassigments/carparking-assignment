# Car Park API

This is an API-only application designed to provide information about nearby car parks, including their availability. It's built using Java and the Spring Boot framework, and it stores data in a PostgreSQL database.

## Installations
* Apache Maven
* Docker
* Docker Compose
* Java version = 17

## Approach and Assumptions

* On startup, the application loads static car park data into the database using loadDataFromCSVToDB() function of CarParkService.

* As part of Synchronous approach, An endpoint is provided for API calls to update car park lot data directly into the database.

* As part of Asynchronous approach, Kafka shines as the backbone for asynchronous communication.Availability updates are seamlessly handled through Kafka messaging, ensuring real-time data synchronization and efficient processing.

* The CarPark dto, is in respect to the json object we get in response from the official gov site [car park availibity](https://api.data.gov.sg/v1/transport/carpark-availability)

* We have used Haversine formula to calculate distance between coordinates as it seemed to be the most efficient and less commutation with respect to our requirements.

* For coordinate conversion third party library **proj4j** is used.

## Running the Application

1. Clone the Repository: Clone this repository to your local machine using Git.

```bash
git clone git@gitlab.com:ttekglobalassigments/sunny_thadhani-carpark_assignment.git
```
or
```bash
git clone https://gitlab.com/ttekglobalassigments/sunny_thadhani-carpark_assignment.git
```

2. Build the Application: Navigate to the project directory and build the application using Maven.

```bash
mvn clean install
```

3. Run Docker Containers: Run docker compose file to build and run all services.

```bash
docker-compose up --build
```

### Update Car Park availability using a endpoint

5. Use a below endpoint url to update the realtime data of car park lots to database

```bash
localhost:8081/carparks/updateCarParkLots
```


### Update Car Park availability using a Kafka

4. Send a request to kafka consumer: Navigate to another terminal and run the following command from kafka producer to send request to kafka consumer

```bash
cat data.json | docker-compose exec -T kafka kafka-console-producer.sh --broker-list kafka:9092 --topic park-availability-topic
```

5. Get nearest car parks: The following api will return all the nearest car parks from your location

```bash
localhost:8081/carparks/nearest?latitude=1.37326&longitude=103.897&page=1&per_page=10
```

##### Note: Request parameter latitude and longitude are compulsory fields while page and per page.

6. Shut down application: In order to shut down the application, we need to run the following command in order to close all services
```bash
docker-compose down --volumes
```

## Project Structure

- /config - Consists of config files like kafka consumer and RestTemplate
- /controller - Consists of endpoint definations
- /dto - Includes all POJO's
- /exception - Includes Global Exception Handler
- /model - Includes entity models
- /repository - Includes model repositories
- /service - Consists of service classes with business logic
- /util - Includes all utils functions used throughout the application


## Trade-offs & Possible Solutions in Design and Implementation

* Synchronous approach of updating data though endpoint, may encounter scalability challenges due to potential latency.Faces performance overhead and risks related to data consistency and reliability
* With loading of this static data at application start up, it may increase the loading time of application
* Using Haversine formula to find nearest location is a decent approach, but it can be replaced with some better approach to enhance the accuracy
* Data Model can be more normalized to separate Car Park static info and dynamic availability of lots to scale the application